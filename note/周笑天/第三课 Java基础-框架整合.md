# 第三课 Java基础-框架整合

## 1.项目构建

第三课主要是讲了一下Java基础方向的框架整合相关的知识，包括基本的项目构建和Maven Profile多环境打包；

因为我们公司是使用Spring Boot结构，所以我对于Spring和Spring Boot还是比较了解的，老师举的基本的项目结构是一个标准的三层模型；

Web（请求处理层）->service(业务逻辑层)->dao（数据持久层）->common（公用组件层）