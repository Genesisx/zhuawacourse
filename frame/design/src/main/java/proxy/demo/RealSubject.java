package proxy.demo;

/**
 * 真实的角色
 *
 * @author zhibai
 */
public class RealSubject implements Subject {
    @Override
    public void request() {
        System.out.println("访问真实主题方法...");
    }
}
