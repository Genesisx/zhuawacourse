package proxy.cglib;

/**
 * 实例类
 *
 * @author zhibai
 */
public class HelloConcrete {

    public String sayHello(String str) {
        return "HelloConcrete: " + str;
    }
}
